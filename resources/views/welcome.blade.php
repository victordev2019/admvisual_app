<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'visual.com') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    {{-- particulas --}}
    <script src="{{ asset('vendor/js/particles.min.js') }}" defer></script>
    <script src="{{ asset('vendor/js/particles-config.js') }}" defer></script>
    <script src="{{ asset('vendor/js/lottie-player.js') }}" defer></script>

</head>
{{-- @if (Route::has('login'))
        <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            @auth
                <a href="{{ url('/dashboard') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Dashboard</a>
            @else
                <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                @endif
            @endauth
        </div>
    @endif --}}

<body class="">
    <div id="particles-js" class="particles-js">
        <div class="content text-center">
            <h1 class="text-white text-3xl uppercase text-center mt-4">sistema de cotizaciones</h1>
            <h2 class="text-white text-3xl text-center">visual.com</h2>
            <div class="flex justify-center">
                <lottie-player src="{{ asset('vendor/json/computer.json') }}" background="transparent" speed="1"
                    style="width: 40%; height: 40%;" loop autoplay>
                </lottie-player>
            </div>
            <div class="my-4 text-white uppercase text-md">
                <a class="mr-8 rounded-md" href="{{ route('login') }}">ingresar</a>
                <a class="ml-8 rounded-md" href="{{ route('register') }}">registro</a>
            </div>
        </div>
    </div>
    {{-- <div class="content">
        <h1 class="text-white text-center text-3xl">visual.com</h1>
    </div> --}}
    <script>
        console.log('hi particles...');
    </script>
</body>

</html>
